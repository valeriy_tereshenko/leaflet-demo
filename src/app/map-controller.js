/**
 * Created by steb on 12/04/2017.
 */
handler.t.MapController = function (layerController, apiKeys) {

    this.setOpenStreetMap = function () {

        // create the tile layer with correct attribution for OpenStreetMap
        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';

        var osm = new L.TileLayer(osmUrl, {
            minZoom: 2, maxZoom: 12
            //, attribution: osmAttrib
        });
        layerController.setBaseLayer(osm);
    };


    this.setBingMap = function (type) { //'Road', 'Aerial', 'AerialWithLabels'

        var layer = L.tileLayer.bing({
            bingMapsKey: apiKeys.bing,
            imagerySet: type
        });
        layerController.setBaseLayer(layer);
    };


    this.setGoogleMap = function (type) { //'roadmap', 'satellite', 'terrain', 'hybrid'

        var googleLayer = new L.gridLayer.googleMutant({
            type: type
        });
        layerController.setBaseLayer(googleLayer);
    };


    this.setMapboxMap = function (type) {

        type = type || 'streets';

        var mapboxTiles = L.tileLayer(
            'https://api.mapbox.com/v4/mapbox.' + type + '/{z}/{x}/{y}.png?access_token=' + apiKeys.mapbox, {
                attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            });

        layerController.setBaseLayer(mapboxTiles);
    };
};