/**
 * Created by steb on 12/04/2017.
 */

handler.t.BidirectionalReference = function () {

    var names = [];
    this['#from'] = function (name, object) {
        validateName(name);

        this[name] = object;
        names.push(name);
    };

    var resolvePromise;
    this['#promise'] = new Promise(function (resolve) {
        resolvePromise = resolve;
    });

    this['#resolve'] = function (name, object) {
        validateName(name);

        var promiseToResolve = resolvePromise;
        resolvePromise = null; // lock for reenter in resolved method

        if (promiseToResolve == null)
            throw new Error('link resolved');

        this[name] = object;
        names.push(name);
        promiseToResolve(this);
    };


    this['#destroy'] = function () {

        for (var i in names)
            delete this[names[i]];

        delete names;
        delete this['#promise'];
        delete this['#resolve'];
    };


    function validateName(name) {
        if (name == '#promise' || name == '#resolve' || name == '#from')
            throw new Error('#promise, #resolve, #from is reserved names');

        if (names.indexOf(name) > -1)
            throw new Error('duplicated name: ' + name);
    }
};