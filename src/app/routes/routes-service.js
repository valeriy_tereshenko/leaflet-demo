/**
 * Created by steb on 13/04/2017.
 */
handler.t.RoutesService = function (Spline) {

    var routes;

    this.getRoutes = function () {
        return [{id: 0, name: 'ASIA SOUTH AMERICA EAST COAST'}];
    };

    this.getRouteData = function (route) {
        return routes[route.id];
    };


    this.getCurvedPath = function(points){

        var arr = [];

        for (var i = 0, l = points.length - 1; i < l; i++) {
            var x = curveSection(points[i], points[i + 1], 0.1);
            arr = arr.concat(x);
        }

        return arr;
    };

    function curveSection(from, to, height) {

        var
            a = (to.lat - from.lat),
            b = (to.lon - from.lon),
            lat = (from.lat + to.lat) / 2,
            lon = (from.lon + to.lon) / 2,
            length = Math.sqrt(a * a + b * b),
            normLat = a / length,
            normLon = b / length;

        var points = [from, {
            lat: lat - normLon * height * length,
            lon: lon + normLat * height * length
        }, to];

        return makeBezier(points, length);
    }

    function makeBezier(points, length) {

        var options = {
            points: points.map(function (pt) {
                return {x: pt.lat, y: pt.lon};
            }),

            duration: length > 0
                ? Math.floor(length * 1000)
                : 1000
        };

        var spline = new Spline(options);

        var coordinates = [];
        for (var i = 0; i < spline.duration; i += 10) {
            var pos = spline.pos(i);

            if (isNaN(pos.x) || isNaN(pos.y))
                return coordinates;

            if (Math.floor(i / 100) % 2 === 0) {
                coordinates.push([pos.x, pos.y]);
            }
        }

        return coordinates;
    }


    routes = [
        [
            {
                name: "Qingdao ^CNQIN",
                lat: 36.03,
                lon: 120.19
            },
            {
                name: "Shanghai ^CNYAH",
                lat: 31.14,
                lon: 121.27
            },
            {
                name: "Ningbo ^CNTMI",
                lat: 29.53,
                lon: 121.33
            },
            {
                name: "Chiwan ^CNCWQ",
                lat: 22.28,
                lon: 113.52
            },
            {
                name: "Singapore^SGSIN",
                lat: 1.16,
                lon: 103.49
            },
            {
                name: "Port Klang^MYKWP",
                lat: 3,
                lon: 101.23
            },
            {
                name: "Itaguai ^BRIGD",
                lat: -22.52,
                lon: -43.46
            },
            {
                name: "Santos ^BREMB",
                lat: -23.55,
                lon: -46.19
            },
            {
                name: "Paranagua ^BRPGU",
                lat: -25.29,
                lon: -48.3
            },
            {
                name: "Buenos Aires^ARTDP",
                lat: -34.46,
                lon: -58.22
            },
            {
                name: "Montevideo^UYTCP",
                lat: -35.54,
                lon: -56.13
            },
            {
                name: "Rio Grande ^BRRGB",
                lat: -32.03,
                lon: -52.05
            },
            {
                name: "Navegantes ^BRNVT",
                lat: -26.53,
                lon: -48.38
            },
            {
                name: "Itapoa ^BRIIP",
                lat: -26.07,
                lon: -48.36
            },
            {
                name: "Santos ^BREMB",
                lat: -23.55,
                lon: -46.19
            },
            {
                name: "Itaguai ^BRIGD",
                lat: -22.52,
                lon: -43.46
            },
            {
                name: "Port Klang^MYKWP",
                lat: 3,
                lon: 101.23
            },
            {
                name: "Singapore^SGSIN",
                lat: 1.16,
                lon: 103.49
            },
            {
                name: "Hong Kong^HKHKG",
                lat: 22.18,
                lon: 114.1
            },
            {
                name: "Hong Kong^HKHTT",
                lat: 22.18,
                lon: 114.1
            }
        ]
    ];
}