/**
 * Created by steb on 18/04/2017.
 */
handler.t.ArcRouteController = function (layerController, routesService, map) {

    this.setPath = function (pathId) {

        var
            data = routesService.getRouteData({id: pathId}),
            curvedPath = routesService.getCurvedPath(data),

            pathLayer = L.polyline(curvedPath, {color: 'green'}),

            pointsGroup = L.layerGroup(
                data.map(function (x) {
                    return L.marker(
                        [x.lat, x.lon],
                        {title: x.name});
                })
            );

        pointsGroup.bindPopup();

        layerController.setPathLayer(pathLayer);
        layerController.setPointsLayer(pointsGroup);

        map.fitBounds(pathLayer.getBounds());
    };
};