/**
 * Created by steb on 13/04/2017.
 */
handler.t.OptionsWindowView = function (link, providersConfig) {

    var
        providerSelect, typeSelect, mapType;


    this.build = function (map) {
        var view = L.control({position: 'topright'});

        view.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'options-window');
            div.innerHTML =
                ('<div class="fond"></div>' +
                '<div class="content">' +
                '   select map provider:<br/>' +
                '   <select>{0}</select>' +
                '   <br/><hr/>' +
                '   <div class="map-type">' +
                '       select map type: <br/>' +
                '       <select></select>' +
                '   </div>' +
                '</div>').format(buildProviders());

            var selects = div.getElementsByTagName('select');
            providerSelect = selects[0];
            providerSelect.onchange = makeOnChange('providersController', 'setProvider');

            typeSelect = selects[1];
            typeSelect.onchange = makeOnChange('mapTypeController', 'selectOption');

            mapType = div.getElementsByClassName('map-type')[0];

            return div;
        };

        view.addTo(map);

    };


    this.setTypeOptions = function (options) {

        var html = '';
        for (var i in options)
            html += '<option value="{0}">{0}</option>'.format(options[i]);

        typeSelect.innerHTML = html;
    };

    this.setTypeSelectVisibility = function (isVisible) {

        mapType.style.display = isVisible ? '' : 'none';
    };


    function makeOnChange(ctrl, action) {
        return function (event) {
            link[ctrl][action](event.target.value);
        }
    }

    function buildProviders() {

        var html = '';
        for (var i in providersConfig)
            html += '<option value="{0}">{0}</option>'.format(i);

        return html;
    }
};