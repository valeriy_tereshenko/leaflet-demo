/**
 * Created by steb on 12/04/2017.
 */
handler.t.ProvidersController = function (providersConfig, mapController, mapTypeController, link) {

    var activeProvider;

    this.setProvider = function (providerName) {

        var options = providersConfig[providerName];
        if (!options)
            throw new Error('No such provider: ' + providerName);

        if (activeProvider == providerName)
            return;

        var setMapMethodName = 'set' + providerName + 'Map';
        mapController[setMapMethodName](options[0]);
        mapTypeController.setOptions(setMapMethodName, options);
    };
};
