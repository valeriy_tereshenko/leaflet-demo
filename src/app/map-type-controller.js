/**
 * Created by steb on 12/04/2017.
 */
handler.t.MapTypeController = function (link, mapController) {

    var activeSetMapMethodName,
        activeOption;

    this.setOptions = function (setMapMethodName, options) {

        var hasOptions = options.length > 0;
        activeSetMapMethodName = setMapMethodName;

        link.optionsWindowView.setTypeSelectVisibility(hasOptions);

        if (hasOptions) {
            link.optionsWindowView.setTypeOptions(options);
            activeOption = options[0];
        }
    };

    this.selectOption = function (option) {

        if (activeOption == option)
            return;

        activeOption = option;
        mapController[activeSetMapMethodName](option);
    };
};