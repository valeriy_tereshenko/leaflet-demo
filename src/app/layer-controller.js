/**
 * Created by steb on 12/04/2017.
 */
handler.t.LayerController = function(map) {

    var local = {
        baseLayer: null,
        pathLayer: null,
        pointsLayer: null,
        layerControl: null
    };


    this.setBaseLayer = function (layer) {

        if (!layer)
            throw new Error('layer required');

        if (local.baseLayer)
            map.removeLayer(local.baseLayer);

        local.baseLayer = layer;
        map.addLayer(layer);
    };


    this.setPointsLayer = function(points){
        
        removeLayer('pointsLayer');
        local.pointsLayer = getLayerControl().addOverlay(points, 'Points');
        points.addTo(map);
    };


    this.setPathLayer = function(path){
        
        removeLayer('pathLayer');
        local.pathLayer = getLayerControl().addOverlay(path, 'Path');
        path.addTo(map);
    };


    function getLayerControl() {

        if (!local.layerControl) {
            local.layerControl = new L.Control.Layers(null, null, {position: 'topleft'});
            local.layerControl.addTo(map);
        }

        return local.layerControl;
    }


    function removeLayer(localName){

        if(!local[localName]) return;

        getLayerControl().removeLayer(localName[localName]);
        delete local[localName];
    }
}