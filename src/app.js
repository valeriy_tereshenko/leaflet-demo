/**
 * Created by steb on 12/04/2017.
 */

var handler = {

    t: { }, // types

    app: {
        /* map, layerController, mapController, optionsWindowView, mapTypeController,
         providersController, routesService, arcRouteController */
    },

    links: {/* toOptionsWindowView */},

    apiKeys: {
        bing: 'AuhiCJHlGzhg93IqUH_oCpl_-ZUrIE6SPftlyGYUvr9Amx5nzA-WqGcPquyFZl4L',
        mapbox: 'pk.eyJ1IjoidmFsZXJhLXRlc3QwMSIsImEiOiJjajFyb2ptOHcwMDBtMnFxcjVpdnEwcHBjIn0.L9qwQPcCoF_IqYR3RKYstA'
    },

    providersConfig: {
        'Bing': ['Road', 'Aerial', 'AerialWithLabels'],
        'Google': ['roadmap', 'satellite', 'terrain', 'hybrid'],
        'OpenStreet': [],
        'Mapbox':['streets', 'satellite', 'mapbox-terrain-v2', 'mapbox-streets-v7', 'mapbox-traffic-v1']
    },

    initMap: function () {
        this.app.map = L.map('map-id')
            .setView([0, 0], 5);

        return this;
    },

    initApp: function () {
        with (this) {
            app.layerController = new t.LayerController(app.map);

            app.mapController = new t.MapController(app.layerController, apiKeys);


            links.toOptionsWindowView = new t.BidirectionalReference();

            app.optionsWindowView = new t.OptionsWindowView(links.toOptionsWindowView, providersConfig);
            links.toOptionsWindowView['#from']('optionsWindowView', app.optionsWindowView);


            app.mapTypeController = new t.MapTypeController(links.toOptionsWindowView, app.mapController);
            links.toOptionsWindowView['#from']('mapTypeController', app.mapTypeController);

            app.providersController = new t.ProvidersController(
                providersConfig, app.mapController, app.mapTypeController, links.toOptionsWindowView);
            links.toOptionsWindowView['#resolve']('providersController', app.providersController);


            app.routesService = new t.RoutesService(t.Spline);

            app.arcRouteController = new t.ArcRouteController(app.layerController,
                app.routesService, app.map);
        }
        return this;
    },

    buildUi: function () {
        with(this.app){
            
            optionsWindowView.build(map);
        }

        return this;
    },

    '#destroy': function () {

    }
};


window.onload = function() {

    handler
        .initMap()
        .initApp()
        .buildUi()
        .app.providersController.setProvider('Bing');

    handler.app
        .arcRouteController.setPath(0);
};